<?php 

class ShopCart
{
    private $AjaxPost;
    Private $ArrayPost;         
    Private $SessionCart;

    function __construct(){
        session_start();
        if($_POST){
            $this->AjaxPost = $_POST;
            $this->ArrayPost = json_decode($this->AjaxPost['json'], true);
            $this->SessionCart = ($_SESSION['cart'] ? $_SESSION['cart'] : array());
            $functionName = $this->AjaxPost['name'];
            
            echo $this->$functionName($this->SessionCart, $this->ArrayPost);
        }
        unset($_POST);
    }

    // Функция добавления товара в корзину

    public function cartAdd($cart, $array){
        $counter = count($cart);
        if($counter > 0){
            $fixProduct = $this->findProduct($cart, $array);
        }else{
            $fixProduct = true;
        }

        if($fixProduct === true && $array['qty'] > 0){
            $counter = count($_SESSION['cart']);
            $_SESSION['cart'][$counter] = $array;
        }
    }

    // Функция вывода всех товаров в корзину

    public function createdCart($cart, $array){
        if(count($cart) !== 0){
            $i = 0;
            foreach($cart as $arr){
                $price = str_replace(['$', '.00', ' '], '', $arr['price']);
                $subtotal = $price * $arr['qty'];
                $substr = strlen($arr['img']) - 5;
                $cartImg = "/assets/img/basket/cart-".substr($arr['img'], $substr);
                $result[$i] = array(
                    'name' => $arr['name'],
                    'img' => $cartImg,
                    'href' => $arr['href'],
                    'price' => "$".$price.".00",
                    'qty' => $arr['qty'],
                    'subtotal' => "$".$subtotal.".00"
                );
                $i++;
            }
            $result = json_encode($result);
        }
        return $result;
    }

    // Функция обновления колличества товаров в корзине на странице продукта

    public function updateProduct($cart, $array){
        $counter = count($cart);
        if($counter !== 0){
            $qty = $this->findUpdateProduct($cart, $array);
            
            if(empty($qty)){
                $qty = 0;
            }

        }else{
            $qty = 0;
        }
        return $qty;
    }
    
    // Функция удаления товара из корзину

    public function deleteProductCart($cart, $array){
        $counter = count($cart);
        if($counter !== 0){
            $this->finddeleteProductCart($cart, $array);
        }  
    }

    public function qtyUpdateProduct($cart, $array){
        $counter = count($cart);
        if($counter !== 0){
           $output = $this->findqtyUpdateProduct($cart, $array);
           $result['subtotal'] = '$'. str_replace(['$', '.00', ' '], '', $output['price']) * $output['qty'].'.00' ;
           $result = json_encode($result);
        }   
        return $result;
    }

    // Функция удаления всей корзины 

    public function clearCart(){
        unset($_SESSION['cart']);
    }
    
    // Вспомогательные функции
    
    private function findProduct($cart, $array){
        foreach($cart as $key => $val){
            if($val['href'] === $array['href']){
                foreach($val as $name => $value){
                    if($value !== $array[$name]){
                        $_SESSION['cart'][$key][$name] = $array[$name];
                    }
                }
                if($_SESSION['cart'][$key]['qty'] < 1){
                    unset($_SESSION['cart'][$key]);
                    $_SESSION['cart'] = array_values($_SESSION['cart']);
                }
                return false;
                exit();
            };
        }
        return true;
    }

    private function findUpdateProduct($cart, $array){
        foreach($cart as $key => $arr){
            if($arr['href'] === $array['href']){
                $qty = $cart[$key]['qty'];
                return $qty;
                exit();
            }
        }
        return false;
    }

    private function finddeleteProductCart($cart, $array){
        foreach($cart as $key => $arr){
            if($cart[$key]['href'] === $array['href']){
                unset($_SESSION['cart'][$key]);
                $_SESSION['cart'] = array_values($_SESSION['cart']);
                return true;
                exit();
            }
        };
        return false;
    }

    private function findqtyUpdateProduct($cart, $array){
        foreach($cart as $key => $arr){
            if($cart[$key]['href'] === $array['href']){
                if($cart[$key]['qty'] !== $array['qty']){
                    $_SESSION['cart'][$key]['qty'] = $array['qty'];
                };
                return $_SESSION['cart'][$key];
                exit();
            }
        };
        return false;
    }
}

return new ShopCart();
